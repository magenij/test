package main
import (
 "log"
 "net/http"
// "os"
)

type server struct{}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    switch r.Method {
    case "GET":
        w.WriteHeader(http.StatusOK)
        w.Write([]byte(`{"message": "200 - Here is your data."}`))
    default:
        w.WriteHeader(http.StatusNotFound)
        w.Write([]byte(`{"message": "404 - Not Found :("}`))
    }
}

func main() {
    s := &server{}
    http.Handle("/", s)
//    port := ":" + os.Getenv("PORT") // Dosn't use right now. Solution for heroku
    log.Fatal(http.ListenAndServe(":8080", nil))
}
